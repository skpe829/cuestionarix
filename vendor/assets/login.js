$(document).ready(function(){
	$("#btnLogin").on("click", function(e){ e.preventDefault(); login(); })
});

function login(){
	var data = new Object();
	data.txtUsuario = $("#txtUsuario").val();
	data.txtPass = $("#txtPassword").val();

	$.ajax({
		url:base_url+"./Personas/getLogin",
		type:"POST",
		dataType:"JSON",
		data: data,
		success: function(rs){
			if(rs[0]){
				location.href=rs[1];
			}else{
				alert(rs[1])
			}
		}
	})
}