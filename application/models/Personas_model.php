<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personas_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function setRegistro($data){
		$insert = array("per_nomb" => $data['nombres'],
						"per_apel" => $data['apellidos'],
						"per_cedu" => $data['cedula'],
						"per_corr" => $data['correo'],
						"per_pass" => password_hash($data['password'], PASSWORD_DEFAULT)
						);
		$this->db->set("per_cddt",'now()',FALSE);
		$this->db->set("per_updt",'now()',FALSE);
		$this->db->set("per_lsdt",'now()',FALSE);
		$this->db->trans_begin();
		$this->db->insert("personas",$insert);
		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback();
		    return false;
		}else{
			$this->db->trans_complete();
			return true;
		}
	}

	public function getRegistroByMail($data){
		$this->db->select('*');
		$this->db->from('personas');
		$this->db->where('per_corr', $data);
		$query = $this->db->get();
		return $query->Row();
	}

	public function getRegistros(){
		$where = "(MOD(per_id, 3) <> 0 AND per_id <> 1)";
		$this->db->where($where);
		$rs = $this->db->get('personas');

		if($rs) return $rs->Result();
		else return false;
	}

	public function delRegistroById($data){
		$this->db->trans_begin();
			$this->db->where('per_id', $data);
			$this->db->delete('personas');
		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback();
		    return false;
		}else{
			$this->db->trans_complete();
			return true;
		}
	}

	public function actualizarAcceso($data){
		$this->db->trans_begin();
			$this->db->set("per_lsdt",'now()',FALSE);
			$this->db->where('per_id', $data);
			$this->db->update('personas');
		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback();
		}else{
			$this->db->trans_complete();
		}


	}
}
