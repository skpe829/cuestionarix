<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function index(){
		$this->load->view('common/head_view');
		$this->load->view('Inicio_view');
		$this->load->view('common/foot_view');
	}

	public function Desconectar(){
		$this->session->sess_destroy();
		redirect('Inicio');
	}
}
