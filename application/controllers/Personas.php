<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personas extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Personas_model');
	}

	public function setRegistro(){
		echo json_encode($this->Personas_model->setRegistro($this->input->post()));
	}

	public function getRegistrobyId(){
		echo json_encode($this->Personas_model->getRegistrobyId($this->input->post()));
	}

	public function login(){
		$this->load->view('common/head_view');
		$this->load->view('Login_view');
		$this->load->view('common/foot_view');
	}

	public function getLogin(){
		$registro = $this->Personas_model->getRegistrobyMail($this->input->post('txtUsuario'));
		if($registro){
			if(password_verify($this->input->post('txtPass'), $registro->per_pass)){
				$usuario_data = array(
						'id' => $registro->per_id,
						'nom_usr' => $registro->per_nomb,
						'usr_prv' => ($registro->per_id % 3 == 0 || $registro->per_id == 1 ) ? "Admin":"User",
						'login' => TRUE);
				$this->session->set_userdata($usuario_data);
				$this->Personas_model->actualizarAcceso($registro->per_id);
				echo json_encode(array(true, base_url("./Personas/Lista")));
			}
		}else{
			echo json_encode(array(false,"Error el usuario no esta registrado/clave incorrecta"));
		}
	}

	public function Lista(){
		$this->load->view('common/head_view');
		$data['lista'] = $this->Personas_model->getRegistros();
		$this->load->view('Lista_view',$data);
		$this->load->view('common/foot_view');
	}
}
