<!DOCTYPE html>
<html>
<head>
	<title>Reto Tecnico</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url('Inicio/'); ?>">Cuestionarix</a>
        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo base_url('Inicio/'); ?>">Registro</a></li>
                <?php
                    if($this->session->userdata('login')) {
                        echo  "<li><a href=".base_url('Personas/Lista').">Lista Personas</a></li>";
                        echo  "<li><a href=".base_url('Inicio/Desconectar').">Desconectar</a></li>";
                    }
                    else echo "<li><a href=".base_url('Personas/login').">Ingreso</a></li>";
                ?>
            </ul>
        </div>
    </div>
</nav>