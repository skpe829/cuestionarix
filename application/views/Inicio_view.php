<div class="container">
	<div class="panel panel-default">
		 <div class="panel-heading">
		 	Registro de Usuarios
		 </div>
		 <div class="panel-body">
		 	<div class="form-group">
		 		<label>Nombres: </label>
		 		<input class="form-control" type="text" name="txtNombre" id="txtNombre" required="">
		 	</div>
		 	<div class="form-group">
		 		<label>Apellidos: </label>
		 		<input class="form-control" type="text" name="txtApellido" id="txtApellido" required="">
		 	</div>
		 	<div class="form-group">
		 		<label>Cedula: </label>
		 		<input class="form-control" type="text" name="txtCed" id="txtCed" required="">
		 	</div>
		 	<div class="form-group">
		 		<label>Correo Electronico: </label>
		 		<input class="form-control" type="email" name="txtEmail" id="txtEmail" required="">
		 	</div>
		 	<div class="form-group">
		 		<label>Contraseña: </label>
		 		<input class="form-control" type="password" name="txtPass" id="txtPass" required="">
		 	</div>
		 </div>
		 <div class="panel-footer">
		 	<button class="btn btn-primary" id="btnRegistrar"> Registrar </button>
		 </div>
	</div>
</div>