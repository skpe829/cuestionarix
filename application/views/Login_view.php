<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Inicio de Sesion</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <input class="form-control" placeholder="Email" name="txtUsuario" id="txtUsuario" type="text" autofocus required="" autocomplete="false">
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="Password" name="txtPassword" id="txtPassword" type="password" value="" required="" autocomplete="false">
                    </div>
                    <button id="btnLogin" class="btn btn-lg btn-primary btn-block">Acceder</button>
                </div>
            </div>
        </div>
    </div>
</div>